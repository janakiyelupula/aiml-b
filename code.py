import tkinter as tk
from tkinter import Tk, Frame, Label, Entry, Button, messagebox,Text,ttk
from PIL import Image, ImageTk
import sqlite3

font_lbl  = ('Courier New',12, 'bold')
root = Tk()
root.title('HOSTEL HELP DESK')
root.geometry('800x600+550+150')
root.iconbitmap(default = "../Assets/logo.ico")



class Home:
    def __init__(self, root):
        self.root = root
        self.root.resizable(False,False)
        conn = sqlite3.connect("UserLogin.db")
        cursor = conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS users (name TEXT PRIMARY KEY, password NUMBER NOT NULL)''')
        conn.close()
        conn = sqlite3.connect("AdminLogin.db")
        cursor = conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS admin (name TEXT PRIMARY KEY, password NUMBER NOT NULL)''')
        conn.close()
        conn = sqlite3.connect("Data.db")
        cursor = conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS data (serialNo INTEGER PRIMARY KEY AUTOINCREMENT,id NUMBER NOT NULL,name TEXT NOT NULL,hostelName TEXT NOT NULL,roomNo NUMBER NOT NULL,problem TEXT NOT NULL)''')
        conn.close()

        self.frame()
    def frame(self):
        self.fra = Frame(root, width = 800, height = 600, bg = 'white')
        self.fra.place(x = 0, y = 0)
        self.user_logo=Image.open("../Assets/nammu5.jpg") 
        self.user_logo=self.user_logo.resize((800,600))
        self.user_logo=ImageTk.PhotoImage(self.user_logo)
        self.user_logo_lbl=Label(self.fra,image=self.user_logo)
        self.user_logo_lbl.place(x=0,y=0)
        self.start_btn = Button(self.fra, text = 'START', font = ('Courier New',18, 'bold'), command = self.signup,bg="green",width=10,height=1)
        self.start_btn.place(x = 600, y = 500)
        self.wel = Label(self.fra, text  = 'WELCOME TO HOSTEL HELP DESK', font = ('Courier New',15, 'bold'), bg = 'maroon', fg = 'white',height=2,width=40)
        self.wel.place(x = 170, y = 50)

        
    def signup(self):
        self.fra.destroy()
        self.root.title("signup")
        self.user_frame = Frame(root, width = 800, height = 600, bg = 'white')
        self.user_frame.place(x = 0, y = 0)
        
        self.user_logo = Image.open('../Assets/nammu1.jpg')
        self.user_logo = self.user_logo.resize((100, 100))

        self.user_logo = ImageTk.PhotoImage(self.user_logo)
        self.user_logo_lbl = Label(self.user_frame, image = self.user_logo)
        self.user_logo_lbl.place(x = 120, y = 75)

        self.user_name = Label(self.user_frame, text  = 'USER NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_name.place(x = 40, y = 200)
        self.user_name_entry = Entry(self.user_frame, width = 15)
        self.user_name_entry.place(x = 170, y = 200)

        self.user_pass = Label(self.user_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 10)
        self.user_pass.place(x = 40, y = 240)
        self.user_pass_entry = Entry(self.user_frame, width = 15, show = '*')
        self.user_pass_entry.place(x = 170, y = 240)

        self.user_btn = Button(self.user_frame, text = 'SIGNUP', font = font_lbl,command = self.register_user)
        self.user_btn.place(x = 120, y = 300)

        self.lgn1_btn = Button(self.user_frame, text = 'LOGIN', font = font_lbl, command = self.login)
        self.lgn1_btn.place(x = 120, y = 350)


        self.admin_frame = Frame(root, width = 800, height = 600, bg = 'white')
        self.admin_frame.place(x = 300, y = 0)

        self.admin_logo = Image.open('../Assets/nammu2.jpg')
        self.admin_logo = self.admin_logo.resize((100, 100))

        self.admin_logo = ImageTk.PhotoImage(self.admin_logo)
        self.admin_logo_lbl = Label(self.admin_frame, image = self.admin_logo)
        self.admin_logo_lbl.place(x = 250, y = 75)

        self.admin_name = Label(self.admin_frame, text  = 'ADMIN NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.admin_name.place(x = 130, y = 200)
        self.admin_name_entry = Entry(self.admin_frame, width = 15)
        self.admin_name_entry.place(x = 270, y = 200)

        self.admin_pass = Label(self.admin_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 11)
        self.admin_pass.place(x = 130, y = 240)
        self.admin_pass_entry = Entry(self.admin_frame, width = 15, show = '*')
        self.admin_pass_entry.place(x = 270, y = 240)

        self.admin_btn = Button(self.admin_frame, text = 'SIGNUP', font = font_lbl, command = self.register_admin)
        self.admin_btn.place(x = 280, y = 300)

        self.lgn2_btn = Button(self.admin_frame, text = 'LOGIN', font = font_lbl, command = self.login)
        self.lgn2_btn.place(x = 280, y = 350)

        self.back_btn = Button(self.user_frame, text = 'BACK', font = font_lbl, command = self.frame,bg="red")
        self.back_btn.place(x = 30, y = 30)

    def register_user(self):
        name = self.user_name_entry.get()
        password = self.user_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("UserLogin.db")
            cursor = conn.cursor()
            cursor.execute('SELECT name FROM users WHERE name=?', [name])
            if cursor.fetchone() is not None:
                messagebox.showerror("Error", "Username already exists.")
            else:
                cursor.execute('INSERT INTO users VALUES (?,  ?)', [name, password])
                conn.commit()
                conn.close()
                messagebox.showinfo("Success", "Account has been created.")
                self.login()
        else:
            messagebox.showerror("Error", "Enter all data.")

    def register_admin(self):
        name = self.admin_name_entry.get()
        password = self.admin_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("AdminLogin.db")
            cursor = conn.cursor()
            cursor.execute('SELECT name FROM admin WHERE name=?', [name])
            if cursor.fetchone() is not None:
                messagebox.showerror("Error", "Username already exists.")
            else:
                cursor.execute('INSERT INTO admin VALUES (?,  ?)', [name, password])
                conn.commit()
                conn.close()
                messagebox.showinfo("Success", "Account has been created.")
                self.login()
        else:
            messagebox.showerror("Error", "Enter all data.")


    def login(self):
        self.fra.destroy()
        self.root.title("Login")
        self.user_frame = Frame(root, width = 800, height = 600, bg = 'white')
        self.user_frame.place(x = 0, y = 0)
        
        self.user_logo = Image.open('../Assets/nammu1.jpg')
        self.user_logo = self.user_logo.resize((100, 100))

        self.user_logo = ImageTk.PhotoImage(self.user_logo)
        self.user_logo_lbl = Label(self.user_frame, image = self.user_logo)
        self.user_logo_lbl.place(x = 120, y = 75)

        self.user_name = Label(self.user_frame, text  = 'USER NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_name.place(x = 40, y = 200)
        self.user_name_entry = Entry(self.user_frame, width = 15)
        self.user_name_entry.place(x = 170, y = 200)

        self.user_pass = Label(self.user_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 10)
        self.user_pass.place(x = 40, y = 240)
        self.user_pass_entry = Entry(self.user_frame, width = 15, show = '*')
        self.user_pass_entry.place(x = 170, y = 240)

        self.user_btn = Button(self.user_frame, text = 'LOGIN', font = font_lbl,command = self.verify_user_login)
        self.user_btn.place(x = 120, y = 300)


        self.admin_frame = Frame(root, width = 800, height = 600, bg = 'white')
        self.admin_frame.place(x = 300, y = 0)

        self.admin_logo = Image.open('../Assets/nammu2.jpg')
        self.admin_logo = self.admin_logo.resize((100, 100))

        self.admin_logo = ImageTk.PhotoImage(self.admin_logo)
        self.admin_logo_lbl = Label(self.admin_frame, image = self.admin_logo)
        self.admin_logo_lbl.place(x = 250, y = 75)

        self.admin_name = Label(self.admin_frame, text  = 'ADMIN NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.admin_name.place(x = 130, y = 200)
        self.admin_name_entry = Entry(self.admin_frame, width = 15)
        self.admin_name_entry.place(x = 270, y = 200)

        self.admin_pass = Label(self.admin_frame, text = 'PASSWORD', font = font_lbl, bg = 'steel blue', fg = 'white', width = 11)
        self.admin_pass.place(x = 130, y = 240)
        self.admin_pass_entry = Entry(self.admin_frame, width = 15, show = '*')
        self.admin_pass_entry.place(x = 270, y = 240)

        self.admin_btn = Button(self.admin_frame, text = 'LOGIN', font = font_lbl, command = self.verify_admin_login)
        self.admin_btn.place(x = 280, y = 300)

        self.back_btn = Button(self.user_frame, text = 'BACK', font = font_lbl, command = self.frame,bg="red")
        self.back_btn.place(x = 30, y = 30)

    def verify_user_login(self):
        name = self.user_name_entry.get()
        password = self.user_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("UserLogin.db")
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM users WHERE name=? AND password=?', (name, password))
            user = cursor.fetchone()
            conn.close()
            if user:
                messagebox.showinfo("Success", f"Welcome, {name}!")
                self.user_login()  
            else:
                messagebox.showerror("Error", "Invalid username or password.")
        else:
            messagebox.showerror("Error", "Enter both username and password.")

    def verify_admin_login(self):
        name = self.admin_name_entry.get()
        password = self.admin_pass_entry.get()
        if name and password:
            conn = sqlite3.connect("AdminLogin.db")
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM admin WHERE name=? AND password=?', (name, password))
            user = cursor.fetchone()
            conn.close()
            if user:
                messagebox.showinfo("Success", f"Welcome, {name}!")
                self.admin_login()  
            else:
                messagebox.showerror("Error", "Invalid username or password.")
        else:
            messagebox.showerror("Error", "Enter both username and password.")

    def user_login(self):
        self.root = root
        self.root.title('HOSTEL HELP DESK - USER DASHBOARD')
        self.user_dashboard_left_frame  = Frame(root, width  = 800, height = 600, bg = 'light yellow')
        self.user_dashboard_left_frame.place(x = 0, y = 0)
        self.user_name = Label(self.user_dashboard_left_frame , text  = 'USER NAME ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_name.place(x = 20, y = 100)    
        self.username_entry = Entry(self.user_dashboard_left_frame , width = 20)
        self.username_entry.place(x = 150, y = 100)
        self.user_id = Label(self.user_dashboard_left_frame , text  = 'ID NO ', font = font_lbl, bg = 'steel blue', fg = 'white')
        self.user_id.place(x = 20, y = 190)  
        self.user_id_entry = Entry(self.user_dashboard_left_frame , width = 15)
        self.user_id_entry.place(x = 150, y = 190)
        self.hostelname = Label(self.user_dashboard_left_frame , text  = 'HOSTEL NAME ', font = ('Courier New',11, 'bold'), bg = 'steel blue', fg = 'white')
        self.hostelname.place(x = 20, y = 280)    
        self.hostelnameentry = Entry(self.user_dashboard_left_frame , width = 15)
        self.hostelnameentry.place(x = 150, y = 280)
        self.room = Label(self.user_dashboard_left_frame , text  = 'ROOM NO' , font = font_lbl, bg = 'steel blue', fg = 'white')
        self.room.place(x = 20, y = 370)  
        self.room_entry = Entry(self.user_dashboard_left_frame , width = 15)
        self.room_entry.place(x = 150, y = 370)
        self.pro_lbl = Label( text  = 'PROBLEM ', font = font_lbl, bg = 'red', fg = 'white')
        self.pro_lbl.place(x = 600, y = 80)
        self.text_widget=Text(root,height=10,width=20)
        self.text_widget.place(x=500,y=120)
        sub_button=tk.Button(root,text="SUBMIT",font=font_lbl,bg="green" ,command=self.savedata)
        sub_button.place(x=600,y=400)
        out_button=tk.Button(root,text="LOGOUT",font=font_lbl,bg="red",command=self.login)
        out_button.place(x=330,y=500)

    def savedata(self): 
        uname = self.username_entry.get()
        id=self.user_id_entry.get()
        hname=self.hostelnameentry.get()
        rno=self.room_entry.get()
        prob=self.text_widget.get(1.0,'end-1c')
        if uname and id and hname and rno and prob:
            conn = sqlite3.connect("Data.db")
            cursor = conn.cursor()
            cursor.execute('INSERT INTO data (id, name, hostelName, roomNo, problem) VALUES (?, ?, ?, ?, ?)', [id, uname, hname, rno, prob])
            conn.commit()
            conn.close()
            messagebox.showinfo("Success", "Data saved.")
            self.user_login()
        else:
            messagebox.showerror("Error", "Enter all data.")
    def admin_login(self):
        self.root.title('HOSTEL HELP DESK - ADMIN DASHBOARD')
        self.admin_dashboard_left_frame = tk.Frame(root, width=800, height=600, bg='white')
        self.admin_dashboard_left_frame.place(x=0, y=0)
        out_button = tk.Button(root, text="LOGOUT", font=('Arial', 12), bg="red", command=self.login)
        out_button.place(x=400, y=30)
        conn = sqlite3.connect("Data.db")
        cursor = conn.cursor()
        cursor.execute('''SELECT * FROM data''')
        table = cursor.fetchall()
        conn.close()
        tree_frame = tk.Frame(root, width=700, height=400)
        tree_frame.place(x=10, y=10)
        tree = ttk.Treeview(tree_frame, columns=[column[0] for column in cursor.description], show="headings")
        for col in cursor.description:
            tree.column(col[0], anchor="center",width=130,stretch=False)
            tree.heading(col[0], text=col[0])
        for row in table:
            tree.insert("", "end", values=row)
        tree.pack()
        self.del_label = tk.Label(self.admin_dashboard_left_frame, text='Delete Row', font=('Arial', 12),
                                  bg='steel blue', fg='white')
        self.del_label.place(x=10, y=280)

        self.del_label_entry = tk.Entry(self.admin_dashboard_left_frame, width=15)
        self.del_label_entry.place(x=130, y=280)

        self.del_btn = tk.Button(self.admin_dashboard_left_frame, text='DELETE', font=('Arial', 12), command=self.delete,
                                 bg="red")
        self.del_btn.place(x=320, y=280)

        self.back_btn = tk.Button(self.admin_dashboard_left_frame, text='BACK', font=('Arial', 12), command=self.login,
                                  bg="red")
        self.back_btn.place(x=410, y=280)

    def delete(self):
        ind = self.del_label_entry.get()
        if ind:
            conn = sqlite3.connect("Data.db")
            cursor = conn.cursor()
            cursor.execute('DELETE FROM data WHERE serialNo = ?',[ind])
            conn.commit()
            conn.close()
            messagebox.showinfo("Success", "Data deleted.")
            self.admin_login()
        else:
            messagebox.showerror("Error", "Enter a valid serial number.")


home = Home(root)
root.mainloop()